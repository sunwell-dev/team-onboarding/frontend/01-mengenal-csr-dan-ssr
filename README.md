# Pengenalan CSR dan SSR

## Daftar Isi
- [Pengenalan CSR (Client-Side Rendering)](#pengenalan-csr)
- [Pengenalan SSR (Server-Side Rendering)](#pengenalan-ssr)
- [Perbandingan Antara CSR dan SSR](#perbandingan)
- [Kapan Menggunakan CSR dan SSR](#kapan-menggunakan)
- [Bacaan Lanjut](#bacaan-lanjut)

## Pengenalan CSR (Client-Side Rendering) <a name="pengenalan-csr"></a>

CSR adalah pendekatan di mana proses rendering halaman web terjadi di sisi klien, yaitu di browser pengguna. Pada CSR, server hanya bertugas menyediakan data dalam bentuk API, sedangkan logika UI dan rendering dilakukan di sisi klien menggunakan JavaScript.

Keuntungan CSR termasuk pengalaman pengguna yang lebih responsif karena halaman dapat segera di-render di browser tanpa harus menunggu respon dari server.

## Pengenalan SSR (Server-Side Rendering) <a name="pengenalan-ssr"></a>

SSR adalah pendekatan di mana proses rendering halaman web terjadi di sisi server sebelum dikirimkan ke browser pengguna. Server akan merender tampilan berdasarkan permintaan klien dan mengirimkan HTML lengkap ke browser.

Keuntungan SSR termasuk waktu muat yang lebih cepat karena pengguna dapat melihat konten awal lebih cepat. Ini juga bermanfaat untuk SEO karena mesin pencari dapat melihat konten halaman secara langsung.

## Perbandingan Antara CSR dan SSR <a name="perbandingan"></a>

- CSR membutuhkan waktu lebih lama untuk menampilkan konten awal kepada pengguna karena rendering dilakukan di browser.
- SSR menghasilkan konten awal yang lebih cepat karena rendering dilakukan di server sebelum dikirim ke browser.
- CSR lebih cocok untuk aplikasi yang sangat interaktif dan kompleks karena logika UI ada di sisi klien.
- SSR lebih cocok untuk aplikasi yang membutuhkan SEO yang kuat karena konten langsung terlihat oleh mesin pencari.

## Kapan Menggunakan CSR dan SSR <a name="kapan-menggunakan"></a>

- Gunakan CSR ketika membangun aplikasi web yang sangat interaktif dan kompleks.
- Gunakan SSR ketika membangun aplikasi web yang membutuhkan SEO yang kuat atau ingin meningkatkan waktu muat halaman awal.

## Bacaan Lanjut <a name="bacaan-lanjut"></a>

Untuk informasi lebih lanjut tentang CSR dan SSR, Anda dapat membaca artikel berikut:
- [Understanding Client-Side Rendering (CSR) vs. Server-Side Rendering (SSR)](https://www.freecodecamp.org/news/what-exactly-is-client-side-rendering-and-hows-it-different-from-server-side-rendering-bd5c786b340d/)
- [Vue SSR Guide](https://ssr.vuejs.org/)
